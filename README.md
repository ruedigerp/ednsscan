
# build docker images 

    docker build -t IMAGENAME . 


# scan one domain

    docker run -it IMAGENAME /data/ednsscan example.com
    # witch dns server software and version check 
    docker run -it IMAGENAME /data/ednsscan example.com version

# scan domain list 

    # one domain per line 
    vim ext/domain.list 
    docker run -it -v $(PWD)/ext:/data/ext IMAGENAME /data/ednslistscan
    # witch dns server software and version check 
    docker run -it -v $(PWD)/ext:/data/ext IMAGENAME /data/ednslistscan version 

