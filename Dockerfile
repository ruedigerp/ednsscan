FROM alpine

# build Cythonized version of dnspython for Python 3 to speed operation on big zones
RUN apk update; apk add nmap bind-tools bash lua nmap-scripts 

# copy scanner files to /usr/local/bin so they are easy to execute
COPY . /data

WORKDIR /data
ENTRYPOINT ["bash"]


